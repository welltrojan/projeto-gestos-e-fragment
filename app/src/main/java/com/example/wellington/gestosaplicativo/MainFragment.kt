package com.example.wellington.gestosaplicativo

import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.main_fragment.*
import android.widget.TextView
import android.view.View.OnLongClickListener






class MainFragment : Fragment() {

    lateinit var mView: View
    lateinit var mButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT ){
            Toast.makeText(activity, "Portrait", Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(activity, "Landscape", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater!!.inflate(R.layout.main_fragment, container, false)

        mButton = mView.findViewById(R.id.pressMe)

        mButton.setOnLongClickListener(OnLongClickListener {

            Toast.makeText(activity, "Clicão", Toast.LENGTH_LONG).show()

            val mp = MediaPlayer.create(activity, R.raw.acabou)
            mp.start()

            true
        })

        return mView
    }

}